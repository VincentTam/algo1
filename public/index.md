---
title: Algo 1er jour
author: <a href="https://git.io/vtam">Vincent Tam</a>
institute: EQL
date: 4 avril 2023
header-includes: |
  <link rel="stylesheet" href="style.css">
---

# Rappel de la définition d'un algorithme

Une suite d'instructions donnée à la machine.

Example : prendre un café

```
AJOUTER DE LA POUDRE A LA TASSE
VERSER DE L'EAU DANS LA TASSE
REMUER LA SOLUTION AVEC UNE CUILLERE
BOIRE LE RESULTAT
```

# Organigramme (n'est plus utilisé)

<pre class="mermaid">
flowchart TD
  a1[ACTION 1] --> test{TEST ?}
  test -->|VRAI| a2[ACTION 2]
  test -->|FAUX| a3[ACTION 3]
  a2 --> a3
</pre>

# Pseudo-code

```
DEBUT
  ACTION 1
  SI TEST VRAI ALORS
    ACTION 2
  FINSI
  ACTION 3
FIN
```

# Types de variables

| Type | Nbr d'octets |
| :--- | -----------: |
| booléen | 1 |
| char | 2 |
| short | 2 |
| int | 4 |
| long | 8 |
| double | 8 |

# Elever un nombre au carré

Cet algo calcule le carré du nombre qui lui fournit l'utilisateur

```
VARIABLES
  unNombre : entier
  sonCarré : entier

DEBUT
  afficher("Quel nombre souhaitez-vous élever au carré ?")
  saisir(unNombre)
  sonCarré ← unNombre × unNombre
  afficher("Le carré de", unNombre, "est", sonCarré)
FIN
```

# Permutations de valeurs

Cet algo permet de permuter des valeurs entrées par l'utilisateur.

```
VARIABLES
  valA, valB, valTemp : réels

DEBUT
  afficher("Tapez deux valeurs")
  saisir(valA, valB)
  valTemp ← valA
  valA ← valB
  valB ← valTemp
  afficher("Voici les valeurs :", valA, valB)
FIN
```

# Instruction SI…ALORS imbriquée

```
SI a = 1 ALORS
  SI a = 2 ALORS
    ACTION 1
  SINON
    ACTION 2
  FINSI
SINON
  ACTION 3
FINSI
```

# Faire un café instantané

Cet algo permet de faire un café instantané.

```
VARIABLES
  quantitéCafé, quantitéEau : réels
  avecSucre : booléen

DEBUT
  afficher("Quelle quantité de café souhaitez-vous ?")
  saisir(quantitéCafé)
  verser(quantitéCafé)
  afficher("Quelle quantité d'eau souhaitez-vous ?")
  saisir(quantitéEau)
  bouillir(quantitéEau)
  mélanger(quantitéCafé, quantitéEau)
  afficher("Voulez-vous du sucre ?")
  saisir(avecSucre)
  verserCafé(quantitéCafé)
  verserEau(quantitéEau)
  SI avecSucre ALORS
    ajouterSucre()
  FINSI
FIN
```

# Afficher le double

Cet algo permet d'afficher le double de la valeur saisie par l'utilisateur si
cette valeur est inférieure à un seuil donné.

```
VARIABLES
  SEUIL : entier ← 10
  val : réels

DEBUT
  afficher("Entrez un entier à doubler.")
  saisir(val)
  SI val < SEUIL
    val ← val × 2
  FINSI
  afficher("Résultat", val)
FIN
```

# Calculer le carré

Cet algo permet de calculer le carré des nombres à partir de 1 jusqu'au nombre
souhaité par l'utilisateur.

```
VARIABLES
  index : entier
  borneSupérieure : entier

DEBUT
  afficher("Jusqu'à quel nombre souhaitez-vous élever au carré ?")
  saisir(borneSupérieure)
  POUR index ← 1 à borneSupérieure [ PAR PAS DE 1 ] FAIRE
    afficher(index × index)
  FINPOUR
FIN
```

# Exos : faire plusieurs cafés

Cet algo permet de faire plusieurs cafés instantanés.

```
VARIABLES
  quantitéCafé, quantitéEau : réels
  avecSucre : réels
  index, nbCafés : entier

DEBUT
  afficher("Combien de tasses de café souhaitez-vous ?")
  saisir(nbCafés)
  POUR index ← 1 à nbCafés [ PAR PAS DE 1 ] FAIRE
    afficher("Entrez la quantité du café")
    saisir(quantitéCafé)
    afficher("Entrez la quantité d'eau")
    saisir(quantitéEau)
    afficher("Voulez-vous du sucre avec ?")
    saisir(avecSucre)
    verserCafé(quantitéCafé)
    verserEau(quantitéEau)
    SI avecSucre ALORS
      ajouterSucre()
    FINSI
  FINPOUR
FIN
```

# Exos : faire le total

Cet algo fait la somme des valeurs données par l'utilisateur, le nombre de
valeurs saisies étant indiqué par l'utilisateur lui-même au départ.

```
VARIABLES
  index, nbValeurs : entiers
  valeurSaisie, résultat : réels

DEBUT
résultat ← 0
afficher("Combien de valeurs souhaitez-vous saisir ?")
saisir(nbValeurs)
POUR index ← 1 à nbValeurs [ PAR PAS DE 1 ] FAIRE
  afficher("Entrez une valeur.")
  saisir(valeurSaisie)
  résultat ← résultat + valeurSaisie
FINPOUR
afficher("La somme des valeurs saisies est égale à", résultat)
FIN
```

# Boucles conditionnelles

Contrairement à une boucle `POUR var ← (valInit) à (valFinal) FAIRE … FIN`, on
ne sait pas *a priori* le nombre d'itérations.

1. `TANT QUE (COND) FAIRE … FINTANTQUE`
1. `REPETER … TANT QUE (COND)`

Différence entre les deux boucles :

1. `TANT QUE (COND) FAIRE … FINTANTQUE` vérifie la `(COND)`ition avant
l'exécution de l'instruction dans l'intérieur de la boucle.  Si `(COND)` n'est
pas satisfaite, l'instruction dedans ne sera pas exécuté.
1. `REPETER … TANT QUE (COND)` exécute l'instruction dedans sans vérifier
`(COND)`.

# Exos : somme de valeurs

Cet algo permet de faire la somme des valeurs saisies par l'utilisateur, tant
que la somme atteinte ne dépasse pas un seuil donné, et quoi qu'il arrive, pas
au delà de 5 valeurs saisies.  Il affiche la somme atteinte et le nombre de
valeurs saisies en fin d'algorithme.

```
VARIABLES
  nbValeurs : entiers
  valeurSaisie, résultat, SEUIL : réels

DEBUT
nbValeurs ← 0
résultat ← 0
SEUIL ← 1000
REPETER
  afficher("Entrez une valeur.")
  saisir(valeurSaisie)
  résultat ← résultat + valeurSaisie
  nbValeurs ← nbValeurs + 1
TANT QUE (résultat ≤ SEUIL ET nbValeurs ≤ 5)
afficher("La somme des valeurs saisies est égale à", résultat)
afficher("Vous avez saisi", nbValeurs, "valeur(s).")
FIN
```

# Code source

<div class="embed-responsive"><object class="code-embed-responsive" type="text/plain"
    data="index.md"></object></div>

# Code source (cont.)

<div class="embed-responsive"><object class="code-embed-responsive" type="text/css"
    data="style.css"></object></div>

<script type="module">
  import mermaid from 'https://cdn.jsdelivr.net/npm/mermaid@10/dist/mermaid.esm.min.mjs';
</script>
